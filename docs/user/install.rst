.. _install:

Installation of odata driver
====================================
Installing ``drb-driver-odata`` with execute the following in a terminal:

.. code-block::

    pip install drb-driver-odata
