from uuid import UUID

from requests.auth import HTTPBasicAuth

from drb.drivers.odata import ODataServiceNodeCSC

svc_url = 'https://my.csc.odata.com'
user = 'user'
password = 'pass'


# Connect with Basic Auth
auth = HTTPBasicAuth(user, password)

# generate ODataServiceNode without authentication
odata = ODataServiceNodeCSC(svc_url)

# get product attributes
uuid = UUID('0723d9bf-02a2-3e99-b1b3-f6d81de84b62')
prd_node = odata[uuid]
attr_node = prd_node['Attributes']['Footprint']

attr_type = attr_node @ 'ValueType'
attr_value = attr_node.value

# For DhuS ValueType not exist in Attributes...
attr_type = attr_node @ 'ContentType'
