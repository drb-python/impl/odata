from uuid import UUID

from drb.topics import resolver
from requests.auth import HTTPBasicAuth

from drb.drivers.odata import ODataServiceNodeCSC

svc_url = 'https://my.csc.odata.com'
user = 'user'
password = 'pass'


# Connect with Basic Auth
auth = HTTPBasicAuth(user, password)

# generate ODataServiceNode without authentication
odata = ODataServiceNodeCSC(svc_url)
# generate ODataServiceNode with authentication mechanism
odata = ODataServiceNodeCSC(svc_url, auth=auth)

# connect using the resolver
url = 'https+odata://my.csc.odata.com'
odata = resolver.create(url)

# total number of children
product_count = len(odata)

# retrieve first ODataProductNode
node_idx = odata[0]

# retrieve last ODataProductNode
node_idx = odata[-1]

# retrieve 10 first products
products = odata[:10]

# retrieve Product by name
name = 'S2B_OPER_MSI_L0__GR_EPAE_..._D05_N02.06.tar'
node_name_list = odata[name]  # returns a list
node_name = odata[name]  # returns first occurrence of the list

# retrieve Product by UUID
uuid = UUID('0723d9bf-02a2-3e99-b1b3-f6d81de84b62')
node_uuid = odata[uuid]
