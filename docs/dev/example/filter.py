from uuid import UUID

from requests.auth import HTTPBasicAuth
from shapely import Polygon

from drb.drivers.odata import ODataServiceNodeCSC, ODataQueryPredicate, ExpressionFunc, ExpressionType

svc_url = 'https://my.csc.odata.com'
user = 'user'
password = 'pass'


# Connect with Basic Auth
auth = HTTPBasicAuth(user, password)

# generate ODataServiceNode without authentication
odata = ODataServiceNodeCSC(svc_url)

# filter and order products
filtered_children = odata[
    ODataQueryPredicate(
        filter="startswith(Name,'S1')",
        order="ContentLength desc",
        top=10,
        skip=50
    )
]

# You can also use the Expression given with this driver
filtered_children = odata[
    ODataQueryPredicate(
        filter=ExpressionFunc.startswith(ExpressionType.property('Name'),'S2'),
        order=(
            ExpressionType.property('ContentLength'),
            ExpressionType.property('desc')
        ),
        top=10,
        skip=50
    )
]

# For geographic query this driver supports:
# String query like:
footprint = ExpressionType.footprint(
    "geography'SRID=4326;Polygon((0.0 0.0,1.0 0.0,1.0 1.0,0.0 1.0,0.0 0.0))'"
)
# return: geography'SRID=4326;Polygon((0.0 0.0,1.0 0.0,1.0 1.0,0.0 1.0,0.0 0.0))'


# List of tuple representing coordinate:
footprint = ExpressionType.footprint(
    [(0, 0), (1, 0), (1, 1), (0, 1), (0, 0)]
)
# return: geography'SRID=4326;Polygon((0.0 0.0,1.0 0.0,1.0 1.0,0.0 1.0,0.0 0.0))'


# A polygon:
footprint = ExpressionType.footprint(
    Polygon([[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]])
)
# return: geography'SRID=4326;Polygon((0.0 0.0,1.0 0.0,1.0 1.0,0.0 1.0,0.0 0.0))'

# For example write the query to calculate the length
length = ExpressionFunc.geo_length(footprint)
# return: geo.length(DirectRoute)
