import io
from uuid import UUID

from drb.drivers.odata import ODataServiceNodeCSC

svc_url = 'https://my.csc.odata.com'

# generate ODataServiceNode without authentication
odata = ODataServiceNodeCSC(svc_url)

# retrieve Product by UUID
uuid = UUID('0723d9bf-02a2-3e99-b1b3-f6d81de84b62')
node_uuid = odata[uuid]

# Get product, and save product
with node_uuid.get_impl(io.BytesIO) as stream:
    with open('product', 'w') as f:
        f.write(stream.read())
    f.close()
