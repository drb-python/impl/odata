import io
import uuid

from requests.auth import HTTPBasicAuth

from drb.drivers.odata import ODataServiceNodeCSC, ODataQueryPredicate

svc_url = 'https://my.csc.odata.com'
user = 'user'
password = 'pass'

# Connect with Basic Auth
auth = HTTPBasicAuth(user, password)

# generate ODataServiceNode without authentication
odata = ODataServiceNodeCSC(svc_url)

# retrieve the first offline product
offline_prd = odata[ODataQueryPredicate(filter="Online eq false")][0]

# Launch order and return an ODataOrderNode
order = offline_prd.order()

# Start to wait and check if all the products ordered are ready (by default check every 1 hour)
order.wait(step=3600)

# Stop the waiting of the product ordered
order.stop()

# Retrieve the order estimate time off arrival
order.order_eta

# retrieve finished order
ready_order = odata.orders[ODataQueryPredicate(filter="Status%20eq%20OData.CSC.JobStatus%27completed%27")][0]

# Retrieve the product corresponding to his order using the property product
ready_order.product.get_impl(io.BytesIO)

# find an order with the status 'in_progress'
in_progress_order = odata.orders[ODataQueryPredicate(filter="Status%20eq%20OData.CSC.JobStatus%27in_progress%27")][0]

# Cancel order
in_progress_order.cancel()

# Retrieve an order with their id
my_order = odata.orders[uuid.UUID('87ba9bd8-ab5c-4695-af55-65bb92448516')]



