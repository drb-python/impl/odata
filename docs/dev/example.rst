.. _example:

Examples
=========

Connect to your server
----------------------
.. literalinclude:: example/connect.py
    :language: python

Download a product
-------------------
.. literalinclude:: example/download.py
    :language: python

Get the attributes of a file
-----------------------------
.. literalinclude:: example/attributes.py
    :language: python

Search products with filter
-----------------------------
.. literalinclude:: example/filter.py
    :language: python

Order a product
----------------
.. literalinclude:: example/order.py
    :language: python