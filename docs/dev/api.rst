.. _api:

Reference API
=============

OdataNode
------------
.. autoclass:: drb.drivers.odata.odata_node.OdataNode
    :members:

ODataServiceNode
-----------------

Represents the OData service.
There are 3 type of service `ODataServiceNodeCSC`,
`ODataServiceNodeDhus` and `ODataServiceNodeDias`. These nodes has no attribute and
has as children Product entities of the service defined by
`ProductNode`.
A specific `ProductNode` can be retrieved during the bracket and
slash navigation by his *Name*(str) or by his *Id*(UUID).

.. autoclass:: drb.drivers.odata.odata_services_nodes.ODataServiceNode
    :members:

ODataServiceNodeCSC
--------------------

.. autoclass:: drb.drivers.odata.odata_services_nodes.ODataServiceNodeCSC
    :members:

ODataServiceNodeDhus
---------------------

.. autoclass:: drb.drivers.odata.odata_services_nodes.ODataServiceNodeDhus
    :members:

ODataServiceNodeDias
--------------------

.. autoclass:: drb.drivers.odata.odata_services_nodes.ODataServiceNodeDias
    :members:

ODataProductNode
-----------------

Represents a Product entity of the OData service.
This node has as attribute properties of the associated
entity and has for unique child a ProductAttributeNode

.. autoclass:: drb.drivers.odata.odata_nodes.ODataProductNode
    :members:

ODataOrderNode
-----------------

Represents a Order entity of the OData service.
This node can be used to retrieve the product order from
a specific service. This node can cancel an order.
For now the property orders only work for the CSC service,
and his not supported on dias service, but you can access
your order on dias services with their id.

.. autoclass:: drb.drivers.odata.odata_nodes.ODataOrderNode
    :members:

ODataProductAttributeNode
--------------------------

This node allowing to represent the navigation link between the Product entity and its attributes.
It has no attribute and has as children Attribute entities associated to the Product entity,
defined by AttributeNode

.. autoclass:: drb.drivers.odata.odata_nodes.ODataProductAttributeNode
    :members:

ODataAttributeNode
-------------------

Represents an Attribute entity. This node has no child and has as attribute
properties associated to the Attribute entity.

.. autoclass:: drb.drivers.odata.odata_nodes.ODataAttributeNode
    :members:

ODataQueryPredicate
--------------------

This predicate allows to retrieve a specific subset of children of an ODataServiceNode.

.. autoclass:: drb.drivers.odata.odata_utils.ODataQueryPredicate
    :members:

OdataServiceType
----------------

This is all the odata service type supported by this drb implementation.

.. autoclass:: drb.drivers.odata.odata_utils.ODataServiceType
      :members:

ExpressionType
----------------

This class give access to all static method to use different type in Odata query.

.. autoclass:: drb.drivers.odata.expression.ExpressionType
      :members:

ComparisonOperator
-------------------

.. autoclass:: drb.drivers.odata.expression.ComparisonOperator
      :members:

LogicalOperator
-------------------

.. autoclass:: drb.drivers.odata.expression.LogicalOperator
      :members:

ArithmeticOperator
-------------------

.. autoclass:: drb.drivers.odata.expression.ArithmeticOperator
      :members:

GroupingOperator
-------------------

.. autoclass:: drb.drivers.odata.expression.GroupingOperator
      :members:

ExpressionFunc
----------------

This is all the odata built-in function that can be used in filter.

.. autoclass:: drb.drivers.odata.expression.ExpressionFunc
      :members: