===================
Data Request Broker
===================
---------------------------------
ODATA driver for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-odata/month
    :target: https://pepy.tech/project/drb-driver-odata
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-odata.svg
    :target: https://pypi.org/project/drb-driver-odata/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-odata.svg
    :target: https://pypi.org/project/drb-driver-odata/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-odata.svg
    :target: https://pypi.org/project/drb-driver-odata/
    :alt: Python Version Support Badge

-------------------

This drb-driver-odata module implements the OData protocol with DRB data model.
It is able to navigate among Product entities of a OData service.

User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

