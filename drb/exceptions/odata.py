from drb.exceptions.core import DrbException


class OdataRequestException(DrbException):
    pass
