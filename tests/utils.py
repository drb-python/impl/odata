import os
import re
import json
import requests
import httpretty

ODATA_CONTENT_TYPE = 'application/json;odata.metadata=minimal'
global cpt
cpt = 0


def start_mock_odata_csc(svc_url: str, auth_required: bool = False):
    prd1_uuid = '0723d9a4-3bbe-305e-b712-5e820058e065'
    prd2_uuid = '0723d9bf-02a2-3e99-b1b3-f6d81de84b62'
    prd3_uuid = '0723ddbc-b0e7-4702-abeb-de257b9f4094'
    order_uuid = '56ac8297-e55e-4682-87c8-059e1e02260c'
    json_format = '$format=json'

    no_csc_md_url = f'{svc_url}/fake-csc/$metadata'
    md_url = f'{svc_url}/$metadata'
    prd_url = f'{svc_url}/Products'
    orders_url = f'{svc_url}/Orders'
    order_url = f'{svc_url}/Orders({order_uuid})'
    order_prd = f'{svc_url}/Orders({order_uuid})/Product'
    launch_order = f'{svc_url}/Products({prd3_uuid})/OData.CSC.Order'
    prd1_url = f'{svc_url}/Products({prd1_uuid})?{json_format}'
    prd1_value_url = f'{svc_url}/Products({prd1_uuid})/$value'
    prd2_url = f'{svc_url}/Products({prd2_uuid})?{json_format}'
    prd3_url = f'{svc_url}/Products({prd3_uuid})?{json_format}'
    len_prd_url = f'{svc_url}/Products/$count'
    attr_url = f'{svc_url}/Products({prd1_uuid})/Attributes?{json_format}'

    name_url = "https://domain.com/csc/NAME/" \
               "Products?$format=json&$filter=Name%20eq%20test"

    # prepare callback http response
    resource_dir = os.path.join(os.path.dirname(__file__), 'resources')
    odata_ct_json = 'application/json;odata.metadata=minimal'
    odata_ct_tar = 'application/x-tar'
    odata_ct_xml = 'application/xml'

    def no_csc_md_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = odata_ct_json
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'no_csc_metadata.xml')) as f:
            data = f.read()
        return 200, headers, data

    def md_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = 'application/xml'
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'csc_metadata.xml')) as f:
            data = f.read()
        return 200, headers, data

    def prd_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = odata_ct_json
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'products.json')) as f:
            data = json.load(f)

        if '$filter' in request.querystring.keys():
            if 'Name' in request.querystring['$filter'][0]:
                match = re.search(r"Name eq '(.*?)'(\)? and .*)?",
                                  request.querystring['$filter'][0])
                if match is None:
                    data['value'] = []
                else:
                    name = match.group(1)
                    data['value'] = [d for d in data['value']
                                     if d['Name'] == name]

        if len(data['value']) > 0:
            if '$skip' in request.querystring.keys():
                skip = int(request.querystring['$skip'][0])
                data['value'] = data['value'][skip:]
            if '$top' in request.querystring.keys():
                top = int(request.querystring['$top'][0])
                if top == 1:
                    data['value'] = [data['value'][0]]
                else:
                    data['value'] = data['value'][:top]

        data['@odata.count'] = len(data['value'])
        return 200, headers, json.dumps(data)

    def orders_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = odata_ct_json
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'orders.json')) as f:
            data = json.load(f)

        if '$filter' in request.querystring.keys():
            if 'Name' in request.querystring['$filter'][0]:
                match = re.search(r"Name eq '(.*?)'(\)? and .*)?",
                                  request.querystring['$filter'][0])
                if match is None:
                    data['value'] = []
                else:
                    name = match.group(1)
                    data['value'] = [d for d in data['value']
                                     if d['Name'] == name]

        if len(data['value']) > 0:
            if '$skip' in request.querystring.keys():
                skip = int(request.querystring['$skip'][0])
                data['value'] = data['value'][skip:]
            if '$top' in request.querystring.keys():
                top = int(request.querystring['$top'][0])
                if top == 1:
                    data['value'] = [data['value'][0]]
                else:
                    data['value'] = data['value'][:top]

        data['@odata.count'] = len(data['value'])
        return 200, headers, json.dumps(data)

    def prd_len_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = 'text/plain'
        headers['odata-version'] = '4.0'
        return 200, headers, '3'

    def prd1_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = odata_ct_json
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'product1.json')) as f:
            data = f.read()
        return 200, headers, data

    def launch_order_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = odata_ct_json
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'order.json')) as f:
            data = f.read()
        return 200, headers, data

    def order_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = odata_ct_json
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'order.json')) as f:
            data = f.read()
        return 200, headers, data

    def prd1_value_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = odata_ct_tar
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'S2B_OPER_MSI_L0__GR_MPS__'
                                             '20170803T123125_'
                                             'S20170803T090229_'
                                             'D12_N02.05.tar')) as f:
            data = f.read()
        return 200, headers, data

    def prd2_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = odata_ct_json
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'product2.json')) as f:
            data = f.read()
        return 200, headers, data

    def prd3_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = odata_ct_json
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'product3.json')) as f:
            data = f.read()
        return 200, headers, data

    def attr_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = odata_ct_json
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'attributes.json')) as f:
            data = f.read()
        return 200, headers, data

    cpt = 0

    def name_callback(request, uri, headers):
        headers['Content-Type'] = odata_ct_json
        headers['odata-version'] = '4.0'
        global cpt
        cpt += 1
        data = "{\"value\":" + f'{cpt}' + "}"
        return 200, headers, data

    # active http mock and register expected URL and associated response
    httpretty.enable()
    httpretty.register_uri(httpretty.GET, no_csc_md_url, no_csc_md_callback)
    httpretty.register_uri(httpretty.GET, md_url, md_callback)
    httpretty.register_uri(httpretty.GET, prd_url, prd_callback)
    httpretty.register_uri(httpretty.GET, orders_url, orders_callback)
    httpretty.register_uri(httpretty.GET, len_prd_url, prd_len_callback)
    httpretty.register_uri(httpretty.GET, order_url, order_callback)
    httpretty.register_uri(httpretty.GET, order_prd, prd1_callback)
    httpretty.register_uri(httpretty.POST, launch_order, launch_order_callback)
    httpretty.register_uri(httpretty.GET, prd1_url, prd1_callback)
    httpretty.register_uri(httpretty.HEAD, prd1_url, '')
    httpretty.register_uri(httpretty.GET, prd1_value_url, prd1_value_callback)
    httpretty.register_uri(httpretty.HEAD, prd1_value_url, '')
    httpretty.register_uri(httpretty.GET, prd2_url, prd2_callback)
    httpretty.register_uri(httpretty.HEAD, prd2_url, '')
    httpretty.register_uri(httpretty.GET, prd3_url, prd3_callback)
    httpretty.register_uri(httpretty.HEAD, prd3_url, '')
    httpretty.register_uri(httpretty.GET, attr_url, attr_callback)
    httpretty.register_uri(httpretty.GET, name_url, name_callback)


def start_mock_odata_dias(svc_url: str, auth_required: bool = False):
    prd1_uuid = 'da4cdfee-9fef-4ab1-b4a2-20f9ec8005bd'
    prd2_uuid = '34416861-c364-44be-8bc9-16829f32cf31'
    prd3_uuid = 'c3f89d3d-3a06-4e42-9903-a5d07ab5c642'
    json_format = '$format=json'
    no_csc_md_url = f'{svc_url}/fake-csc/$metadata'
    md_url = f'{svc_url}/$metadata'
    prd_name_uri = f'{svc_url}/Products?$format=json&$search=' \
                   f'\"name:S2B_MSIL2A_20180328T120349_N0207_R066_' \
                   f'T29VLD_20180328T175102.zip\"'
    prd1_url = f'{svc_url}/Products({prd1_uuid})?{json_format}'
    prd2_url = f'{svc_url}/Products({prd2_uuid})?{json_format}'
    prd3_url = f'{svc_url}/Products({prd3_uuid})?{json_format}'

    len_prd_url = f'{svc_url}/Products/$count'
    attr_url = f'{svc_url}/Products({prd1_uuid})/Metadata?{json_format}'
    name_url = "https://domain.com/csc/NAME/" \
               "Products?$format=json&$filter=Name%20eq%20test"

    # prepare callback http response
    resource_dir = os.path.join(os.path.dirname(__file__), 'resources')
    odata_ct_json = 'application/json;odata.metadata=minimal'

    def no_csc_md_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = odata_ct_json
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'no_csc_metadata.xml')) as f:
            data = f.read()
        return 200, headers, data

    def md_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = 'application/xml'
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'dias_metadata.xml')) as f:
            data = f.read()
        return 200, headers, data

    def prd_len_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = 'text/plain'
        headers['odata-version'] = '4.0'
        return 200, headers, '3'

    def prd1_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = odata_ct_json
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'product_dias1.json')) as f:
            data = f.read()
        return 200, headers, data

    def prd2_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = odata_ct_json
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'product_dias2.json')) as f:
            data = f.read()
        return 200, headers, data

    def prd3_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = odata_ct_json
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'product_dias3.json')) as f:
            data = f.read()
        return 200, headers, data

    def prd_name(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = odata_ct_json
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'product_dias2_value.json')) as f:
            data = f.read()
        if '$search' in request.querystring.keys():
            if "name:test" in request.querystring['$search'][0] \
                    or "34416861-c364-44be-8bc9-16829f32cff" in \
                    request.querystring['$search'][0]:
                data = "{ \"value\": [] }"
        return 200, headers, data

    def attr_callback(request, uri, headers):
        if auth_required and not check_authorization(request):
            return 401, headers, '401 Unauthorized'
        headers['Content-Type'] = odata_ct_json
        headers['odata-version'] = '4.0'
        with open(os.path.join(resource_dir, 'attributes_dias.json')) as f:
            data = f.read()
        return 200, headers, data

    cpt = 0

    def name_callback(request, uri, headers):
        headers['Content-Type'] = odata_ct_json
        headers['odata-version'] = '4.0'
        global cpt
        cpt += 1
        data = "{\"value\":" + f'{cpt}' + "}"
        return 200, headers, data

    # active http mock and register expected URL and associated response
    httpretty.enable()
    httpretty.register_uri(httpretty.GET, no_csc_md_url, no_csc_md_callback)
    httpretty.register_uri(httpretty.GET, md_url, md_callback)
    httpretty.register_uri(httpretty.GET, prd_name_uri, prd_name)
    httpretty.register_uri(httpretty.GET, len_prd_url, prd_len_callback)
    httpretty.register_uri(httpretty.GET, prd1_url, prd1_callback)
    httpretty.register_uri(httpretty.HEAD, prd1_url, '')
    httpretty.register_uri(httpretty.GET, prd2_url, prd2_callback)
    httpretty.register_uri(httpretty.HEAD, prd2_url, '')
    httpretty.register_uri(httpretty.GET, prd3_url, prd3_callback)
    httpretty.register_uri(httpretty.HEAD, prd3_url, '')
    httpretty.register_uri(httpretty.GET, attr_url, attr_callback)
    httpretty.register_uri(httpretty.GET, name_url, name_callback)


def stop_mock_odata_csc():
    httpretty.disable()
    httpretty.reset()


class MySuccessAuth(requests.auth.AuthBase):
    """
    Mock an HTTP authentication mechanism, adding an authorization header to
    the request.

    :param usr: user username
    :type usr: str
    :param pwd: user password
    :type pwd: str
    """

    def __init__(self, usr, pwd):
        self.usr = usr
        self.pwd = pwd

    def __call__(self, request: requests.Request):
        request.headers['Authorization'] = \
            f'Bearer token_{self.usr}_{self.pwd}'
        return request


def check_authorization(req) -> bool:
    for header in str(req.headers).split('\n'):
        r = header.split(': ')
        if 'Authorization' == r[0] and (r[1].startswith('Bearer token_') or
                                        r[1].startswith('Basic ')):
            return True
    return False


dias_products = [
    {
        "@odata.mediaContentType": "application/zip",
        "id": "da4cdfee-9fef-4ab1-b4a2-20f9ec8005bd",
        "name": "S2A_MSIL1C_20220209T105211_N0400_"
                "R051_T30TYM_20220209T144919.zip",
        "creationDate": "2022-02-09T16:52:31.000Z",
        "beginPosition": "2022-02-09T10:52:11.024Z",
        "offline": False,
        "size": 865774579,
        "pseudopath": "OPTICAL/LEVEL-1C/2022/02/09,"
                      "S2/2A/MSI/LEVEL-1C/S2MSI1C/2022/02/09",
        "footprint": "MULTIPOLYGON (((...)))",
        "quicklook": "/9j/4AAQSkZJ...",
        "downloadable": True,
        "evictionDate": None
    },
    {
        "@odata.mediaContentType": "application/zip",
        "id": "34416861-c364-44be-8bc9-16829f32cf31",
        "name": "S2B_MSIL2A_20180328T120349_N0207_"
                "R066_T29VLD_20180328T175102.zip",
        "creationDate": "2018-04-27T12:58:57.000Z",
        "beginPosition": "2018-03-28T12:03:49.027Z",
        "offline": True,
        "size": 843223742,
        "pseudopath": "OPTICAL/LEVEL-2A/2018/03/28,"
                      "S2/2B/MSI/LEVEL-2A/S2MSI2A/2018/03/28",
        "footprint": "MULTIPOLYGON (((...)))",
        "quicklook": "....",
        "downloadable": False,
        "evictionDate": None
    },
    {
        "@odata.context": "$metadata#Products/$entity",
        "@odata.mediaContentType": "application/zip",
        "id": "c3f89d3d-3a06-4e42-9903-a5d07ab5c642",
        "name": "S5P_OFFL_L2__NO2____20200807T092458_"
                "20200807T110627_14599_01_010302_20200811T094537.zip",
        "creationDate": "2020-08-18T14:10:43.000Z",
        "beginPosition": "2020-08-07T09:46:32Z",
        "offline": True,
        "size": 454961183,
        "pseudopath": "ATMOSPHERIC/LEVEL-2/2020/08/07, "
                      "S5P/5P/TROPOMI/LEVEL-2/L2__NO2___/2020/08/07",
        "footprint": "MULTIPOLYGON (((...)))",
        "quicklook": None,
        "downloadable": False,
        "evictionDate": None
    }
]
